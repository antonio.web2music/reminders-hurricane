//
//  Monster+CoreDataProperties.swift
//  Reminders
//
//  Created by Dario Mandarino on 19/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//
//

import Foundation
import CoreData


extension Monster {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Monster> {
        return NSFetchRequest<Monster>(entityName: "Monster")
    }

    @NSManaged public var idCD: Int16
    @NSManaged public var currentHpCD: Int64
    @NSManaged public var tempoHpCD: Int64
    @NSManaged public var nameCD: String?
    @NSManaged public var descriptionCD: String?
    @NSManaged public var maxHpCD: Int64

}
