//
//  InteractionPB+CoreDataProperties.swift
//  Reminders
//
//  Created by Dario Mandarino on 17/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//
//

import Foundation
import CoreData


extension InteractionPB {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<InteractionPB> {
        return NSFetchRequest<InteractionPB>(entityName: "InteractionPB")
    }

    @NSManaged public var datePunchCD: Date?
    @NSManaged public var idPBCD: Int16
    @NSManaged public var hpCD: String?

}
