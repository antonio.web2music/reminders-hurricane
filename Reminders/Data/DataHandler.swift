

import Foundation
import CoreData
import UIKit

class DataHandler {
    
    static func saveData(onContext context: NSManagedObjectContext!){
        do {
            try context!.save()
        } catch {
            print("Failed saving")
        }
    }
    
    static func deleteObject(_ object: NSManagedObject, onContext context: NSManagedObjectContext!, andCommit: Bool){
        context.delete(object)
        if (andCommit){
            saveData(onContext: context)
        }
    }
    
    static func discardChanges(onContext context: NSManagedObjectContext!){
        context!.rollback()
    }
    
    static func allReminders() -> [Reminder] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        return allRemindersOnContext(context)
    }
    
    static func allRemindersOnContext(_ context: NSManagedObjectContext) -> [Reminder] {
        let remindersFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Reminder")
        do {
            let reminders = try context.fetch(remindersFetchRequest)// as! [Reminder]
            return reminders as! [Reminder]
        } catch {
            print("error at context.execute")
            return []
        }
    }
    
    
    static func insertDateFA(context: NSManagedObjectContext,date: Date){
        let dateFA = DataFirstAccess(context: context)
        dateFA.date = date
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchDateFA (context: NSManagedObjectContext) -> [DataFirstAccess]{
        let fetchRequest: NSFetchRequest<DataFirstAccess> = DataFirstAccess.fetchRequest()
        do{
            let dateFA =  try context.fetch(fetchRequest)
            return dateFA
        } catch{
            print("Problema con il fetch della dataFA")
            return []
        }
    }
    
    static func insertInteractionPB(context: NSManagedObjectContext,date: Date,id: Int16,hpPB: String){
        let interactionPB = InteractionPB(context: context)
        interactionPB.datePunchCD = date
        interactionPB.idPBCD = id
        interactionPB.hpCD = hpPB
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchInteractionPB (context: NSManagedObjectContext) -> [InteractionPB]{
        let fetchRequest: NSFetchRequest<InteractionPB> = InteractionPB.fetchRequest()
        do{
            let interactionPB =  try context.fetch(fetchRequest)
            return interactionPB
        } catch{
            print("Problema con il fetch della dataPB")
            return []
        }
    }
    
    
    static func updateInteractionPB(context: NSManagedObjectContext,interactionPB : InteractionPB,date : Date,id: Int16,hpPB: String){
        interactionPB.datePunchCD = date
        interactionPB.hpCD = hpPB
        interactionPB.hpCD = hpPB
        DataHandler.saveData(onContext: context)
    }
    
    static func insertTutorialCheck(context: NSManagedObjectContext,id: Int16){
        let tutorialCheck = TutorialChecks(context: context)
        tutorialCheck.id = id
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchTutorialChecks (context: NSManagedObjectContext) -> [TutorialChecks]{
        let fetchRequest: NSFetchRequest<TutorialChecks> = TutorialChecks.fetchRequest()
        do{
            let tutorialChecks =  try context.fetch(fetchRequest)
            return tutorialChecks
        } catch{
            print("Problema con il fetch dei check")
            return []
        }
    }
    
    static func insertCharacter(context: NSManagedObjectContext,name: String,classPG: String,currentHp: Int64,currentHexp: Int64,maxExp: Int64,maxHp: Int64,lv: Int16){
        let character = Character(context: context)
        character.classCD = classPG
        character.nameCD = name
        character.currentExpCD = currentHexp
        character.currentHpCD = currentHp
        character.maxHpCD = maxHp
        character.maxExpCD = maxExp
        character.lvCD = lv
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchCharacter (context: NSManagedObjectContext) -> [Character]{
        let fetchRequest: NSFetchRequest<Character> = Character.fetchRequest()
        do{
            let pg =  try context.fetch(fetchRequest)
            return pg
        } catch{
            print("Problema con il fetch del pg")
            return []
        }
    }
    
    static func updateCharacter(context: NSManagedObjectContext,character : Character,name: String,classPG: String,currentHp: Int64,currentHexp: Int64,maxExp: Int64,maxHp: Int64,lv: Int16){
        character.classCD = classPG
        character.nameCD = name
        character.currentExpCD = currentHexp
        character.currentHpCD = currentHp
        character.maxHpCD = maxHp
        character.maxExpCD = maxExp
        character.lvCD = lv
        DataHandler.saveData(onContext: context)
    }
    
    static func insertMonster(context: NSManagedObjectContext,name: String,currentHp: Int64,maxHp: Int64,id: Int16,descr: String,tempHp: Int64){
        let monster = Monster(context: context)
        monster.currentHpCD = currentHp
        monster.descriptionCD = descr
        monster.idCD = id
        monster.maxHpCD = maxHp
        monster.nameCD = name
        monster.tempoHpCD = tempHp
        DataHandler.saveData(onContext: context)
    }
    
    static func fetchMonsters (context: NSManagedObjectContext) -> [Monster]{
           let fetchRequest: NSFetchRequest<Monster> = Monster.fetchRequest()
           do{
               let mobs =  try context.fetch(fetchRequest)
               return mobs
           } catch{
               print("Problema con il fetch dei mostri")
               return []
           }
       }
    
    static func updateMonster(context: NSManagedObjectContext,monster : Monster,name: String,currentHp: Int64,maxHp: Int64,id: Int16,descr: String,tempHp: Int64){
        monster.currentHpCD = currentHp
        monster.descriptionCD = descr
        monster.idCD = id
        monster.maxHpCD = maxHp
        monster.nameCD = name
        monster.tempoHpCD = tempHp
        DataHandler.saveData(onContext: context)
    }
}
