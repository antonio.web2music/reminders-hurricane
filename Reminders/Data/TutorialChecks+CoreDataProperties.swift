//
//  TutorialChecks+CoreDataProperties.swift
//  Reminders
//
//  Created by Dario Mandarino on 18/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//
//

import Foundation
import CoreData


extension TutorialChecks {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TutorialChecks> {
        return NSFetchRequest<TutorialChecks>(entityName: "TutorialChecks")
    }

    @NSManaged public var id: Int16

}
