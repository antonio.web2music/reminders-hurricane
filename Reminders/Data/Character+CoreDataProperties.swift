//
//  Character+CoreDataProperties.swift
//  Reminders
//
//  Created by Dario Mandarino on 19/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//
//

import Foundation
import CoreData


extension Character {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Character> {
        return NSFetchRequest<Character>(entityName: "Character")
    }

    @NSManaged public var classCD: String?
    @NSManaged public var nameCD: String?
    @NSManaged public var currentHpCD: Int64
    @NSManaged public var maxHpCD: Int64
    @NSManaged public var currentExpCD: Int64
    @NSManaged public var maxExpCD: Int64
    @NSManaged public var lvCD: Int16

}
