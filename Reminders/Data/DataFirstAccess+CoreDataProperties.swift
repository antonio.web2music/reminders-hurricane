//
//  DataFirstAccess+CoreDataProperties.swift
//  Reminders
//
//  Created by Dario Mandarino on 17/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//
//

import Foundation
import CoreData


extension DataFirstAccess {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DataFirstAccess> {
        return NSFetchRequest<DataFirstAccess>(entityName: "DateFirstAccess")
    }

    @NSManaged public var date: Date?

}
