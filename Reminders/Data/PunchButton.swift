//
//  PunchButton.swift
//  Reminders
//
//  Created by Dario Mandarino on 16/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation
import UIKit

class PunchButton : UIButton{
    
    private var hpLabel = UILabel()
    private var id = Int16()
    
    func setLalbel(label: UILabel){
        hpLabel = label
    }
    
    func setID(ide: Int16){
        id = ide
    }
    
    func getLalbel() -> UILabel{
        return hpLabel
    }
    
    func getID() -> Int16{
        return id
    }
}
