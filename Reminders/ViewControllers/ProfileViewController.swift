//
//  ProfileViewController.swift
//  Reminders
//
//  Created by Dario Mandarino on 20/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation

import UIKit

class CellClass: UITableViewCell {
    
    
}

class ProfileViewController: UIViewController {
    

    @IBOutlet weak var expLabel: UILabel!
    @IBOutlet weak var hpLabel: UILabel!
    @IBOutlet weak var titleButton: UIButton!
    let transparentView = UIView()
    let tableView = UITableView()
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lvPg: UILabel!
    @IBOutlet weak var namePg: UILabel!
    var selectedButton = UIButton()
    
    var dataSource = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context)
        
        imageView.image = UIImage(named: (pg.first?.classCD)!)
        namePg.text = pg.first?.nameCD
        lvPg.text = "Lv: " + String(pg.first!.lvCD)
        hpLabel.text = "HP: " + String(pg.first!.currentHpCD) + "/" + String(pg.first!.maxHpCD)
        expLabel.text = "EXP: " + String(pg.first!.currentExpCD) + "/" + String(pg.first!.maxExpCD)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CellClass.self, forCellReuseIdentifier: "Cell")
    }
    
    func addTransparentView(frames: CGRect) {
        
        let window = UIApplication.shared.keyWindow
        transparentView.frame = window?.frame ?? self.view.frame
        self.view.addSubview(transparentView)
        
        //Metto la pergamena aperta
        let background = UIImageView(frame: CGRect(x: frames.origin.x - 60, y: frames.origin.y - 30, width: transparentView.frame.width/2, height:  250))
        background.image = UIImage(named: "old-scroll")
        transparentView.addSubview(background)
        
        //Ho cambiato la costruzione. voglio che avvenga sopra il bottone, non sotto
        tableView.frame = CGRect(x: frames.origin.x - 60 , y: frames.origin.y , width: self.view.frame.width/2, height: 0)
        
        self.view.addSubview(tableView)
        tableView.layer.cornerRadius = 5
        
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        tableView.reloadData()
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(removeTransparentView))
        transparentView.addGestureRecognizer(tapgesture)
        transparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 1
            self.tableView.frame = CGRect(x: frames.origin.x - 60, y: frames.origin.y , width: self.view.frame.width/2, height: 200)
        }, completion: nil)
    }
    
    @objc func removeTransparentView() {
        let frames = selectedButton.frame
        UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.tableView.frame = CGRect(x: frames.origin.x - 60, y: frames.origin.y, width: frames.width, height: 0)
        }, completion: nil)
    }

    
    @IBAction func titleButtonAction(_ sender: Any) {

        dataSource = []
        selectedButton = titleButton
        addTransparentView(frames: titleButton.frame)
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = dataSource[indexPath.row]
        //cell.backgroundColor = UIColor(cgColor: CGColor(srgbRed: <#T##CGFloat#>, green: <#T##CGFloat#>, blue: <#T##CGFloat#>, alpha: 1))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedButton.setTitle(dataSource[indexPath.row], for: .normal)
        removeTransparentView()
    }
}
