//
//  FirstWeekTutorialViewController.swift
//  Reminders
//
//  Created by Dario Mandarino on 18/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class FirstWeekTutorialVC: UIViewController{
    
    @IBAction func tapButton(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        DataHandler.insertTutorialCheck(context: context, id: 1)
        performSegue(withIdentifier: "endFirstTutorial", sender: self)
    }
}
