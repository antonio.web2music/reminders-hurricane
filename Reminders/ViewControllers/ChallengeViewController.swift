//
//  ChallengeViewController.swift
//  Reminders
//
//  Created by Dario Mandarino on 15/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation
import UIKit


class ChallengeViewController: UIViewController{
    
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var nameAndLevel: UILabel!
    @IBOutlet weak var hpLabel: UILabel!
    @IBOutlet weak var expLabel: UILabel!
    
    override func viewDidLoad() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //Dobbiamo fare il fetch della settimana in cui ci troviamo
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context)
        let dateFA = DataHandler.fetchDateFA(context: context)
        let tutorialChecks = DataHandler.fetchTutorialChecks(context: context)
        
        var idFirstWeek = 1
        var idSecondWeek = 2
        
        nameAndLevel.text = pg.first!.nameCD! + " Lv:" + String(pg.first!.lvCD)
        hpLabel.text = "HP: " + String(pg.first!.currentHpCD) + "/" + String(pg.first!.maxHpCD)
        expLabel.text = "EXP: " + String(pg.first!.currentExpCD) + "/" + String(pg.first!.maxExpCD)
        
        expLabel.font = UIFont(name: "FallingnSkyBlack-GYXA", size: 18)
        expLabel.textColor = .white
        hpLabel.font = UIFont(name: "FallingnSkyBlack-GYXA", size: 18)
        hpLabel.textColor = .white
        nameAndLevel.font = UIFont(name: "FallingnSkyBlack-GYXA", size: 18)
        nameAndLevel.textColor = .white
        
        
        if(daysBetween(start: (dateFA.first?.date)!, end: Date()) < 8){
            for tutorialCheck in tutorialChecks{
                if(tutorialCheck.id == idFirstWeek){
                    idFirstWeek = 0
                }
            }
            if(idFirstWeek == 1){
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "firstWeekVC") as! FirstWeekTutorialVC
                self.present(newViewController, animated: true, completion: nil)
            }
            
            //generazione task se siamo nella prima settimana
            firstWeek()
        }
            
        else if((daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 8) && (daysBetween(start: (dateFA.first?.date)!, end: Date()) < 18)){
            for tutorialCheck in tutorialChecks{
                if(tutorialCheck.id == idSecondWeek){
                    idSecondWeek = 0
                }
            }
            if(idSecondWeek == 2){
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "firstWeekVC") as! FirstWeekTutorialVC
                self.present(newViewController, animated: true, completion: nil)
            }
            secondWeek()
        }
        else if((daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 14) && (daysBetween(start: (dateFA.first?.date)!, end: Date()) < 21)){
            
            thirdWeek()
        }
        else if(daysBetween(start: (dateFA.first?.date)!, end: Date()) >= 21){
            
            fourthWeek()
        }
        
        //L'altezza della stack va gestita dinamicamente, serve mettere un constraint per far funzionare la scroll
        var altezza = CGFloat(integerLiteral: 0)
        for view1 in stack.subviews{
            altezza = altezza + view1.frame.height + 8
        }
        //non so di preciso quanti elementi avrò nella mia stack: dipendono dalla settimana
        let heightConstraint = NSLayoutConstraint(item: stack, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: altezza)
        
        stack.addConstraint(heightConstraint)
        
    }
    
    
    
    func firstWeek(){
        //la prima settimana avremo tre mostri semplici, con 1 hp ciascuno. Li genero e li aggiungo alla stack
        monsterlv1(Y: -30,id: 0)
        monsterlv1(Y: Int(view.frame.height)/4 - 30, id: 1)
        monsterlv1(Y: Int(view.frame.height)/2 - 30, id: 2)
    }
    
    func secondWeek(){
        
    }
    
    func thirdWeek(){
        
    }
    
    func fourthWeek(){
        
    }
    
    func monsterlv1(Y: Int,id: Int16){
        
        let calendar = Calendar.current
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let monsters = DataHandler.fetchMonsters(context: context)
        let interactions = DataHandler.fetchInteractionPB(context: context)
        var found = false
        
        //questa è la view che deve essere aggiunta alla stack
        let view1 = UIView(frame: CGRect(x: 0,y: Y,width: Int(view.frame.width), height: Int(view.frame.height/4)))
        //prima di poterla aggiungere è il caso di inserire tutti gli elementi necessari
        //al bottone ho dovuto mettere un id per poter memorizzare quale è stato toccato, ho dovuto creare la classe PunchButton
        let button1 = PunchButton(frame: CGRect(x: 20, y: 124, width: 130, height: 130))
        let hpLabel = UILabel(frame: CGRect(x: 193, y: 180, width: 221, height: 64))
        let title = UILabel(frame: CGRect(x: 81, y: 54, width: 252, height: 66))
        let description = UILabel(frame: CGRect(x: 193, y: 128, width: 221, height: 64))
        
        title.textAlignment = .center
        title.textColor = .white
        title.font = UIFont(name: "FallingSky", size: 32)
        hpLabel.textColor = .white
        hpLabel.font = UIFont(name: "FallingSkyBlack-GYXA", size: 20)
        description.textColor = .white
        description.font = UIFont(name: "FallingSkyBlack-GYXA", size: 20)
       
        //se non è la prima volta che apro questa sezione il mostro sarà gia stato creato, lo fetcho con il suo stato attuale
        for monster in monsters{
            if(monster.idCD == id){
                found = true
                hpLabel.text = "HP:" + " " + String(monster.tempoHpCD) + "/" + String(monster.maxHpCD)
                button1.setImage(UIImage(named: monster.nameCD!), for: .normal)
                button1.setImage(UIImage(named: monster.nameCD!), for: .highlighted)
                title.text = monster.nameCD
                description.text = monster.descriptionCD
            }
        }
        
        //se è il mio primo avvio devo instaziare il mostro nel database
        if(!found){
            switch id {
            case 0:
                DataHandler.insertMonster(context: context, name: "Alarm Monster", currentHp: 70, maxHp: 70, id: 0, descr: "Set your alarm at 07:00", tempHp: 70)
                hpLabel.text = "HP:" + " " + String(70) + "/" + String(70)
                button1.setImage(UIImage(named: "Alarm Monster"), for: .normal)
                button1.setImage(UIImage(named: "Alarm Monster"), for: .highlighted)
                title.text = "Alarm Monster"
                description.text = "Set your alarm at 07:00"
            case 1:
                DataHandler.insertMonster(context: context, name: "Water Monster", currentHp: 100, maxHp: 100, id: 1, descr: "Drink a glass of water", tempHp: 100)
                hpLabel.text = "HP:" + " " + String(100) + "/" + String(100)
                button1.setImage(UIImage(named: "Water Monster"), for: .normal)
                button1.setImage(UIImage(named: "Water Monster"), for: .highlighted)
                title.text = "Water Monster"
                description.text = "Drink a glass of water"
            case 2:
                DataHandler.insertMonster(context: context, name: "Breakfast Monster", currentHp: 50, maxHp: 50, id: 2, descr: "Eat healty breakfast", tempHp: 50)
                hpLabel.text = "HP:" + " " + String(50) + "/" + String(50)
                button1.setImage(UIImage(named: "Breakfast Monster"), for: .normal)
                button1.setImage(UIImage(named: "Breakfast Monster"), for: .highlighted)
                title.text = "Breakfast Monster"
                description.text = "Eat healty breakfast"
            default:
                print("id non valido")
            }
        }
        
        button1.setLalbel(label: hpLabel)
        button1.setID(ide: id)
        button1.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        //devo verificare se sono state già fatte delle interazioni oggi e se ha fatto le challenge il giorno prima
        if(interactions.count > 0){
            for inter in interactions{
                if(inter.idPBCD == id){
                    if(calendar.component(.year, from: inter.datePunchCD!) == calendar.component(.year, from: Date()) && calendar.component(.month, from: inter.datePunchCD!) == calendar.component(.month, from: Date()) && calendar.component(.day, from: inter.datePunchCD!) == calendar.component(.day, from: Date())){
                        print("Second attempt today, button blocked")
                        button1.isEnabled = false
                    }
                    else{
                        for mob in monsters{
                            if(mob.idCD == id){
                                if(mob.currentHpCD != mob.tempoHpCD){
                                    print("tutt appost ieri ha fatt a challenge")
                                    DataHandler.updateMonster(context: context, monster: mob, name: mob.nameCD!, currentHp: mob.tempoHpCD, maxHp: mob.maxHpCD, id: mob.idCD, descr: mob.descriptionCD!, tempHp: mob.tempoHpCD)
                                }
                                else{
                                    print("il bastardo ieri non ha fatto la challenge")
                                }
                            }
                        }
                    }
                }
            }
        }
        
        view1.addSubview(hpLabel)
        view1.addSubview(button1)
        view1.addSubview(title)
        view1.addSubview(description)
        
        stack.addSubview(view1)
    }
    
    @objc func buttonAction(sender: PunchButton!) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let pg = DataHandler.fetchCharacter(context: context)
        let monsters = DataHandler.fetchMonsters(context: context)
        let dmg = pg.first!.lvCD * 10
        
        let strings1 = sender.getLalbel().text!.components(separatedBy: " ")
        let strings2 = strings1[1].components(separatedBy: "/")
        
        let currenthp = Int16(strings2.first!)! - dmg
        
        if(currenthp <= 0){
            for mob in monsters{
                if(mob.idCD == sender.getID()){
                    DataHandler.updateMonster(context: context, monster: mob, name: mob.nameCD!, currentHp: mob.currentHpCD, maxHp: mob.maxHpCD, id: mob.idCD, descr: mob.descriptionCD!, tempHp: 0)
                }
            }
            sender.getLalbel().text = "0/" + strings2[1]
        }
        else{
            for mob in monsters{
                if(mob.idCD == sender.getID()){
                    DataHandler.updateMonster(context: context, monster: mob, name: mob.nameCD!, currentHp: mob.currentHpCD, maxHp: mob.maxHpCD, id: mob.idCD, descr: mob.descriptionCD!, tempHp: Int64(currenthp))
                }
            }
            sender.getLalbel().text = "HP:" + " " + String(currenthp) + "/" + strings2[1]
        }
        
        DataHandler.updateCharacter(context: context, character: pg.first!, name: (pg.first?.nameCD)!, classPG: (pg.first?.classCD)!, currentHp: pg.first!.currentHpCD, currentHexp: pg.first!.currentExpCD + 100, maxExp: pg.first!.maxExpCD, maxHp: pg.first!.maxHpCD, lv: pg.first!.lvCD)
        expLabel.text = "EXP: " + String(pg.first!.currentExpCD) + "/" + String(pg.first!.maxExpCD)
        sender.isEnabled = false
        
        let interactions = DataHandler.fetchInteractionPB(context: context)
        var flag = 0
        for inter in interactions{
            if(inter.idPBCD == sender.getID()){
                DataHandler.updateInteractionPB(context: context, interactionPB: inter, date: Date(), id: sender.getID(), hpPB: sender.getLalbel().text!)
                flag = 1
            }
        }
        
        if(flag == 0){
            DataHandler.insertInteractionPB(context: context, date: Date(), id: sender.getID(), hpPB: sender.getLalbel().text!)
        }
    }
    
    func daysBetween(start: Date, end: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
}
