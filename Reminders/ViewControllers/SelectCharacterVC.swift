//
//  SelectCharacterVC.swift
//  Reminders
//
//  Created by Dario Mandarino on 18/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation
import UIKit

class SelectCharacterVC: UIViewController{
    
    @IBOutlet weak var druidButton: PunchButton!
    @IBOutlet weak var archerButton: PunchButton!
    @IBOutlet weak var barbarianButton: PunchButton!
    @IBOutlet weak var mageButton: PunchButton!
    
    var idPicked = Int16()
    
    override func viewWillAppear(_ animated: Bool) {
        druidButton.setID(ide: 1)
        archerButton.setID(ide: 2)
        barbarianButton.setID(ide: 3)
        mageButton.setID(ide: 4)
    }
    
    
    @IBAction func buttonTapped(_ sender: PunchButton) {
        idPicked = sender.getID()
        self.performSegue(withIdentifier: "characterPicked", sender: sender)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "characterPicked" {
            if let nextViewController = segue.destination as? AddNameCharacterVC {
                nextViewController.idCharacter = idPicked
            }
        }
    }
}
