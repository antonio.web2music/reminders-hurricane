//
//  OnboardingViewController.swift
//  Ritual
//
//  Created by Delia Cavalli on 05/05/2020.
//  Copyright © 2020 Delia Cavalli. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var enterButton: UIButton!
    
    var scrollWidth: CGFloat! = 0.0
    var scrollHeight: CGFloat! = 0.0
    
    var headings = ["CREATE NEW GOOD RITUALS", "BE CONSTANT", "IMPROVE YOUR WELL-BEING"]
    var subheadings = ["Create a series of good habits to improve aspects of your day", "Constancy is a very important part of your journey to get the results you hope for, every day you will have to follow the goals that come to you", "Choose a path of good habits to follow and have fun"]
    var images = ["1", "2", "3"]
    
    //get dynamic width and height of scrollview and save it
    
    override func viewDidLayoutSubviews() {
        scrollWidth = scrollView.frame.size.width
        scrollHeight = scrollView.frame.size.height
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        
        //to call viewDidLayoutSubviews() and get dynamic width and height of scrollview
        
        self.scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        //crete the slides and add them
        
        var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        
        for index in 0..<headings.count {
            frame.origin.x = scrollWidth * CGFloat(index)
            frame.size = CGSize(width: scrollWidth, height: scrollHeight)
            
            let slide = UIView(frame: frame)
            
            //subviews
            
            
            let imageView = UIImageView.init(image: UIImage.init(named: images[index]))
            imageView.frame = CGRect(x:0,y:0,width:120,height:120)
            imageView.contentMode = .scaleAspectFit
            imageView.center = CGPoint(x:scrollWidth/2,y: scrollHeight/2 - 100)
            
            let text1 = UILabel.init(frame: CGRect(x:32,y:imageView.frame.maxY-220,width:scrollWidth-64,height:30))
            text1.textAlignment = .center
            text1.font = UIFont.boldSystemFont(ofSize: 20.0)
            text1.text = headings[index]
            
            let text2 = UILabel.init(frame: CGRect(x:32,y:text1.frame.maxY+20,width:scrollWidth-64,height:50))
            text2.textAlignment = .center
            text2.numberOfLines = 3
            text2.font = UIFont.systemFont(ofSize: 18.0)
            text2.text = subheadings[index]
            
            imageView.image = UIImage(named: "Sleep")
            
            slide.addSubview(imageView)
            slide.addSubview(text1)
            slide.addSubview(text2)
            scrollView.addSubview(slide)
            
        }
        
        //set width of scrollview to accomodate all the slides
        
        scrollView.contentSize = CGSize(width: scrollWidth * CGFloat(headings.count), height: scrollHeight)
        
        //disable vertical scroll/bounce
        
        self.scrollView.contentSize.height = 1.0
        
        //initial state
        
        pageControl.numberOfPages = headings.count
        pageControl.currentPage = 0
        
    }
    
    //indicator
    
    @IBAction func pageChanged(_ sender: Any) {
        scrollView!.scrollRectToVisible(CGRect(x: scrollWidth * CGFloat ((pageControl?.currentPage)!), y: 0, width: scrollWidth, height: scrollHeight), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setIndiactorForCurrentPage()
    }
    
    func setIndiactorForCurrentPage()  {
        let page = (scrollView?.contentOffset.x)!/scrollWidth
        pageControl?.currentPage = Int(page)
        
        
        if(pageControl?.currentPage == 2){
            
            enterButton.titleLabel?.textColor = .black
            enterButton.isEnabled = true
        }
    }
    
    // Do any additional setup after loading the view.
}


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */



