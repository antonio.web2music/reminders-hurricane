//
//  AddNameCharacterVC.swift
//  Reminders
//
//  Created by Dario Mandarino on 18/05/2020.
//  Copyright © 2020 Kyrill Cousson. All rights reserved.
//

import Foundation
import UIKit

class AddNameCharacterVC: UIViewController{
    
    var idCharacter = Int16()
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var classCharacter: UILabel!
    
    @IBOutlet weak var textField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch idCharacter {
        case 1:
            imageView.image = UIImage(named: "Druid")
            classCharacter.text = "Druid"
            
        case 2:
            imageView.image = UIImage(named: "Archer")
            classCharacter.text = "Archer"
        case 3:
            imageView.image = UIImage(named: "Barbarian")
            classCharacter.text = "Barbarian"
        case 4:
            imageView.image = UIImage(named: "Mage")
            classCharacter.text = "Mage"
        default:
            print("Non è arrivato il dato")
        }
        
        classCharacter.font = UIFont(name: "FallingnSkyBlack-GYXA", size: 18)
        //classCharacter.
        classCharacter.textColor = .white
    }
    
    
    @IBAction func okTapped(_ sender: Any) {
        
        if(textField.text != ""){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
            DataHandler.insertCharacter(context: context, name: textField.text!, classPG: classCharacter.text!, currentHp: 100, currentHexp: 0, maxExp: 1000, maxHp: 100, lv: 1)
        performSegue(withIdentifier: "goToTutorial", sender: self)
        }
        else{
            textField.text = "PLS write your name"
        }
    }
}
