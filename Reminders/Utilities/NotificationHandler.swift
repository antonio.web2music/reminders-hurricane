

import Foundation
import UserNotifications
import CoreLocation
import CoreData
import UIKit

class NotificationHandler {
    
    private static let notificationCenter = UNUserNotificationCenter.current()
    
    static func setupNotifications(){
        
        notificationCenter.removeAllDeliveredNotifications()
        notificationCenter.removeAllPendingNotificationRequests()
        
        let reminders = DataHandler.allReminders()
        for reminder in reminders {
            if (reminder.done == false) {
                addNotificationFromReminder(reminder)
            }
        }
    }
    
    
    static func requestNotificationAuthorization(){
        
        let options: UNAuthorizationOptions = [.alert, .sound]
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
    }
    
    static func addNotificationFromReminder(_ reminder: Reminder){
        
        let content = UNMutableNotificationContent()
        
        content.title = reminder.title!
        content.sound = UNNotificationSound.default
        content.badge = 1
        
        if let dueDate = reminder.dueDate {
            addScheduledNotification(identifier: reminder.id!.uuidString, date: dueDate as Date, content: content)
        }
        else if let location = reminder.location { // TODO: allow both scheduled + location-based reminders
            let region = CLCircularRegion.init(center: CLLocationCoordinate2D.init(latitude: location.latitude, longitude: location.longitude), radius: location.radius, identifier: reminder.id!.uuidString)
            region.notifyOnEntry = true
            addGeofenceNotification(identifier: reminder.id!.uuidString, region: region, content: content)
        }
    }
    
    static func removeReminderNotification(reminder: Reminder){
        notificationCenter.removePendingNotificationRequests(withIdentifiers: [(reminder.id?.uuidString)!])
        notificationCenter.removeDeliveredNotifications(withIdentifiers: [(reminder.id?.uuidString)!])
    }
    
    private static func addScheduledNotification(identifier: String, date: Date, content: UNMutableNotificationContent){
        
        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
    private static func addGeofenceNotification(identifier: String, region: CLRegion, content: UNMutableNotificationContent){
        
        let trigger = UNLocationNotificationTrigger(region:region, repeats:true)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
    func addObjectiveNotification (identifier: String){
        if(identifier == "perdere_peso_id"){
            NotificationHandler.loseWeightNotification()
        }
    }
    
    private static func loseWeightNotification(){
        
        let content = UNMutableNotificationContent()
       
        content.title = "Chiattò"
        content.body = "Non ta può magnà a pizz"
        content.sound = UNNotificationSound.default
        content.badge = 1
        
        var dateComponents1 = DateComponents()
        dateComponents1.hour = 9
        dateComponents1.minute = 30
        let trigger1 = UNCalendarNotificationTrigger(dateMatching: dateComponents1, repeats: true)
        
        var dateComponents2 = DateComponents()
        dateComponents2.hour = 9
        dateComponents2.minute = 32
        let trigger2 = UNCalendarNotificationTrigger(dateMatching: dateComponents2, repeats: true)
        
        var dateComponents3 = DateComponents()
        dateComponents3.hour = 9
        dateComponents3.minute = 33
        let trigger3 = UNCalendarNotificationTrigger(dateMatching: dateComponents3, repeats: true)
        
        let request1 = UNNotificationRequest(identifier: "loseWeight1", content: content, trigger: trigger1)
        notificationCenter.add(request1) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
        let request2 = UNNotificationRequest(identifier: "loseWeight2", content: content, trigger: trigger2)
        notificationCenter.add(request2) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
        let request3 = UNNotificationRequest(identifier: "loseWeight3", content: content, trigger: trigger3)
        notificationCenter.add(request3) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
}


//iLHoVPurjDQx7MzkfL7z
